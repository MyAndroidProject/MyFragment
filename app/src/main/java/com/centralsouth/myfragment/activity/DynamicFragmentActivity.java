package com.centralsouth.myfragment.activity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.centralsouth.myfragment.R;
import com.centralsouth.myfragment.fragment.ArticleFragment;
import com.centralsouth.myfragment.fragment.HeadFragment;

public class DynamicFragmentActivity extends FragmentActivity implements HeadFragment.HeadFragmentInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_fragment);
        Button button = (Button) findViewById(R.id.btnClick);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment();
            }
        });
        if (findViewById(R.id.frlContainer) != null) {
            if (savedInstanceState != null) {
                return;
            }
            ArticleFragment firstFragment = new ArticleFragment();
            //新建自定fragment
            firstFragment.setArguments(getIntent().getExtras());
            //设置自定fragment参数
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frlContainer, firstFragment).commit();
            //获得fragmentmanager，产生事务并提交。
        }
    }

    @Override
    public void sendData(String data) {
        System.out.print(data);
        Toast.makeText(this,data,Toast.LENGTH_LONG).show();
        //此处接收数据
    }

    private void replaceFragment() {
        HeadFragment headFragment = new HeadFragment();
        //新建fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //获取事务
        fragmentTransaction.replace(R.id.frlContainer, headFragment);
        //替换
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        //提交事务
    }
}
