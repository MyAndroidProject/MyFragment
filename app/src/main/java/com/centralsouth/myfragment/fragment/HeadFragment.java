package com.centralsouth.myfragment.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.centralsouth.myfragment.R;


/**
 * Created by boleng on 1/4/17.
 */

public class HeadFragment extends Fragment {

    private HeadFragmentInterface headFragmentInterface;

    public  interface HeadFragmentInterface{
        public void sendData(String data);
    }

    private Button btnSend;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_head,container,false);
        btnSend= (Button) view.findViewById(R.id.btnSend);
        
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headFragmentInterface.sendData("who are you");
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        headFragmentInterface= (HeadFragmentInterface) context;
        //转化activity到fragment接口
        super.onAttach(context);
    }
}
