package com.centralsouth.myfragment.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.centralsouth.myfragment.R;


/**
 * Created by boleng on 1/3/17.
 */

public class LeftFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_left,container,false);
        //将布局文件转化成view,传入的container是父view，传入false代表设置该view的大小是父view的大小
    }
}
