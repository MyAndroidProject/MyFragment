package com.centralsouth.myfragment.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.centralsouth.myfragment.R;

public class StaticFragmentActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_fragment);
    }
}
